Venus has been manufacturing commercial and residential products since 1965. Our 11,000 square foot warehouse is located in Tempe (Phoenix), Arizona. We service national hotel, motel, and school accounts, as well as residential customers.

Address: 707 E Curry Rd, Tempe, AZ 85281, USA

Phone: 402-968-6778

Website: https://www.venusmanufacturing.com